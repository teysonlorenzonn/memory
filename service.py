import random

class service:
    def __init__(self, name_archive):
        self.name_archive = name_archive
        self.memory = []
        self.quant_it_ram = 0
        self.quant_it_sam = 0

    def read_archive(self):
        with open(self.name_archive, 'r') as arq:
            archive = arq.read()
        self.memory = archive.split('\n')

    def searchRAM(self, metch, quant=0):
        x = random.randint(0, len(self.memory)-1)
        quant += 1

        if self.memory[x] == metch:
            self.quant_it_ram = quant
            return True
        else:
            return self.searchRAM(metch, quant)

    def searchSAM(self, metch):
        for i in range(len(self.memory)):
            pos_number = self.memory[i]
            if pos_number == metch:
                self.quant_it_sam = i
                return self.searchRAM(metch)
        return False

    def media(self, type):
        if type == 'sam':
            return self.quant_it_sam / len(self.memory)
        return self.quant_it_ram / len(self.memory)

    def qaunt_it(self, type):
        if type == 'sam':
            return self.quant_it_sam
        return self.quant_it_ram

    def length(self):
        return len(self.memory)