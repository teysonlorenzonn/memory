from service import service
import sys

def continueProgram():
    continue_program = input('\nQuer executar novamente?(s/N)').upper()
    if continue_program == 'S':
        main()
    else:
        exit()

def main():
    sys.setrecursionlimit(100000)
    read = service('arq.txt')
    read.read_archive()

    try:
        metch = input('\nDigite uma palavra para ser buscada: ')
        verify = read.searchSAM(metch)

        if not verify:
            print('\nPalavra não encontrada na memória')
            continueProgram()

        print('\n{0} tentativas de busca em {1} posições endereçadas na memória RAM para achar a palavra {2}'.format(read.qaunt_it('ram'), read.length(), metch))
        print('{0} tentativas de busca em {1} posições endereçadas na memória SAM para achar a palavra {2}'.format(read.qaunt_it('sam'), read.length(), metch))

        print('\nA média de iterações da memória RAM é de: {0}'.format(round(read.media('ram'), 4)))
        print('A média de iterações da memória SAM é de: {0}'.format(round(read.media('sam'), 4)))

    except Exception as error:
        print('\n{0}\n'.format(error))
        main()
    continueProgram()

main()
